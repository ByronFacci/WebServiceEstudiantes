package com.example.byronmendoza.practicaws.rest.service;

import com.example.byronmendoza.practicaws.rest.constants.ApiConstanst;
import com.example.byronmendoza.practicaws.rest.modelo.Estudiantes;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface EstudentService {

    @GET(ApiConstanst.ESTUDIANTES)
    Call<List<Estudiantes>> getEstudiantes();

    @GET(ApiConstanst.ESTUDIANTE)
    Call<Estudiantes> getEstudiante(@Path("id")String id);


}
