package com.example.byronmendoza.practicaws.ui.center;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.byronmendoza.practicaws.R;
import com.example.byronmendoza.practicaws.rest.adapter.EstudiantesAdapter;
import com.example.byronmendoza.practicaws.rest.modelo.Estudiantes;
import com.squareup.picasso.Picasso;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity {

    TextView ide, nombres2, apellidos2, parcial12, parcial22, aprueba2;

    ImageView img2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        String id = getIntent().getStringExtra("id");

        ide = (TextView)findViewById(R.id.IdEstudiantes2);
        nombres2 = (TextView)findViewById(R.id.TxtNombres2);
        apellidos2 = (TextView)findViewById(R.id.TxtApellidos2);
        parcial12 = (TextView)findViewById(R.id.TxtParcial12);
        parcial22 = (TextView)findViewById(R.id.TxtParcial22);
        aprueba2 = (TextView)findViewById(R.id.TxtAprueba2);
        img2 = (ImageView)findViewById(R.id.ImgEst2);

        Estmostrar2(id);


    }

    private void Estmostrar2(String id){
        EstudiantesAdapter estudiantesAdapter = new EstudiantesAdapter();
        Call<Estudiantes> estudiantesCall = estudiantesAdapter.getEstudiante(id);
        estudiantesCall.enqueue(new Callback<Estudiantes>() {
            @Override
            public void onResponse(Call<Estudiantes> call, Response<Estudiantes> response) {
                Estudiantes estudiantes = response.body();
                Log.e("Estudiante", estudiantes.getNombres());

                ide.setText("ID: " + estudiantes.getId().toString());
                nombres2.setText("NOMBRES: " + estudiantes.getNombres().toString());
                apellidos2.setText("APELLIDOS: " + estudiantes.getApellidos().toString());
                parcial12.setText("PARCIAL 1: " + estudiantes.getParcial_uno().toString());
                parcial22.setText("PARCIAL 2: " + estudiantes.getParcial_dos().toString());
                aprueba2.setText("APRUEBA: " + estudiantes.getAprueba().toString());
                Picasso.get().load(estudiantes.getImagen()).into(img2);
            }

            @Override
            public void onFailure(Call<Estudiantes> call, Throwable t) {

            }
        });
    }

}
