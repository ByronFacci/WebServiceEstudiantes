package com.example.byronmendoza.practicaws.ui.center;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.byronmendoza.practicaws.R;
import com.example.byronmendoza.practicaws.rest.adapter.EstudiantesAdapter;
import com.example.byronmendoza.practicaws.rest.modelo.Estudiantes;
import com.example.byronmendoza.practicaws.rest.service.EstudentService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    ArrayList<Estudiantes> ListaEstudiante;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListaEstudiante=new ArrayList<>();
        recyclerView = (RecyclerView)findViewById(R.id.Restudiantes);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        EstMostrar();

    }

    private void EstMostrar() {

        EstudiantesAdapter estudiantesAdapter = new EstudiantesAdapter();
        Call<List<Estudiantes>> call = estudiantesAdapter.getEstudiantes();
        call.enqueue(new Callback<List<Estudiantes>>() {

            @Override
            public void onResponse(Call<List<Estudiantes>> call, Response<List<Estudiantes>> response) {
            List<Estudiantes>List = response.body();

            for (Estudiantes estudiant: List){

                ListaEstudiante.add(estudiant);
            }
            AdaptadorEstudiantes adaptadorEstudiantess = new AdaptadorEstudiantes(ListaEstudiante);
            recyclerView.setAdapter(adaptadorEstudiantess);
            }

            @Override
            public void onFailure(Call<List<Estudiantes>> call, Throwable t) {

            }
        });
    }
}
