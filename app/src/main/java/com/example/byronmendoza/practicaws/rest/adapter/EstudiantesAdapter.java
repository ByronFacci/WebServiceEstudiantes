package com.example.byronmendoza.practicaws.rest.adapter;

import com.example.byronmendoza.practicaws.rest.constants.ApiConstanst;
import com.example.byronmendoza.practicaws.rest.modelo.Estudiantes;
import com.example.byronmendoza.practicaws.rest.service.EstudentService;

import java.util.List;

import retrofit2.Call;

public class EstudiantesAdapter extends BaseAdapter implements EstudentService  {


    private EstudentService estudentService;

    public EstudiantesAdapter(){
        super(ApiConstanst.BASE_ESTUDIANTES_URL);
        estudentService=createService(EstudentService.class);
    }

    @Override
    public Call<List<Estudiantes>> getEstudiantes() {
        return estudentService.getEstudiantes();
    }

    @Override
    public Call<Estudiantes> getEstudiante(String id) {
        return estudentService.getEstudiante(id);
    }
}
