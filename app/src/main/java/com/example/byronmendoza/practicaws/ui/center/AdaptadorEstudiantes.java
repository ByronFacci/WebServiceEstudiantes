package com.example.byronmendoza.practicaws.ui.center;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.byronmendoza.practicaws.R;
import com.example.byronmendoza.practicaws.rest.modelo.Estudiantes;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class AdaptadorEstudiantes extends RecyclerView.Adapter<AdaptadorEstudiantes.ViewEstudiantes>{

    ArrayList<Estudiantes> Est;

    public AdaptadorEstudiantes(ArrayList<Estudiantes> Est){
        this.Est=Est;
    }

    @NonNull
    @Override
    public ViewEstudiantes onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);

        return new ViewEstudiantes(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewEstudiantes holder, final int position) {

        holder.id.setText("ID: "+ Est.get(position).getId());
        holder.Nombres.setText("Nombres: "+ Est.get(position).getNombres());
        holder.Apellidos.setText("Apellidos: " + Est.get(position).getApellidos());
        holder.Parcial1.setText("Parcial: " + Est.get(position).getParcial_dos());
        holder.Parcial2.setText("Parcial: " + Est.get(position).getParcial_dos());
        holder.Aprueba.setText("Aprueba: " +Est.get(position).getAprueba());

        Picasso.get().load(Est.get(position).getImagen()).into(holder.imagen);

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Context context = view.getContext();
                Intent intent = new Intent(context, Main2Activity.class);
                intent.putExtra("id", Est.get(position).getId());
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return Est.size();
    }

    public class ViewEstudiantes extends RecyclerView.ViewHolder{

        TextView id, Nombres, Apellidos, Parcial1, Parcial2, Aprueba;

        ImageView imagen;

        LinearLayout linearLayout;

        public ViewEstudiantes(View itemView) {

            super(itemView);

            id = (TextView)itemView.findViewById(R.id.IdEstudiantes);
            Nombres = (TextView)itemView.findViewById(R.id.TxtNombres);
            Apellidos = (TextView)itemView.findViewById(R.id.TxtApellidos);
            Parcial1 = (TextView)itemView.findViewById(R.id.TxtParcial1);
            Parcial2 = (TextView)itemView.findViewById(R.id.TxtParcial2);
            Aprueba = (TextView)itemView.findViewById(R.id.TxtAprueba);
            imagen = (ImageView)itemView.findViewById(R.id.ImgEst);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.Rlinear);

        }
    }
}
